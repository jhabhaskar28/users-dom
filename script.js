fetch('https://fakestoreapi.com/users')
.then((response) => {
    return response.json();
})
.then((data) => {
    let loader = document.querySelector('.loaderLogo');
    loader.style.display = 'none';

    if(data === null || (Array.isArray(data)===true && data.length === 0)){

        let noUsers = document.createElement('h1');
        noUsers.className = "noUsers";
        noUsers.textContent = "Sorry! No users to display";

        let body = document.querySelector('body');
        let footer = document.querySelector('.footer');

        body.insertBefore(noUsers,footer);

    } else if(Array.isArray(data)){

        data.forEach((userData) => {
            createUser(userData);
        });

    } else {
        createUser(data);
    }

    let header = document.querySelector('header');
    header.style.display = 'flex';

    let footer = document.querySelector('footer');
    footer.style.display = 'flex';
})
.catch((err) => {

    let loader = document.querySelector('.loaderLogo');
    loader.style.display = 'none';

    let failedPageLoad = document.querySelector('.failedPageLoad');
    failedPageLoad.style.display = 'flex';

    let body = document.querySelector('body');
    body.style.backgroundColor = "White";

    console.error(err);
});


function createUser(data){

    let userDiv = document.createElement('div');
    userDiv.className = "user";

    let userFullName = document.createElement('div');
    userFullName.className = "userFullName";
    let firstName = document.createElement('h1');
    firstName.textContent = data.name.firstname.charAt(0).toUpperCase() + data.name.firstname.substring(1);  
    let lastName = document.createElement('h1');
    lastName.textContent = data.name.lastname.charAt(0).toUpperCase() + data.name.lastname.substring(1);  
    userFullName.appendChild(firstName);
    userFullName.appendChild(lastName);
    

    let description = document.createElement('div');
    description.className = "description";
    let userID = document.createElement('h3');
    userID.textContent = `ID: ${data.id}`;
    let userName = document.createElement('h3');
    userName.textContent = `Username: ${data.username}`;
    let email = document.createElement('h3');
    email.textContent = `Email: ${data.email}`;
    
    let addressDiv = document.createElement('div');
    addressDiv.className = "address";
    let addressHeading = document.createElement('h3');
    addressHeading.textContent = "Address:";
    let address = document.createElement('p');
    address.textContent = `${data.address.number}, ${data.address.street}, ${data.address.city}, ${data.address.zipcode}, (${data.address.geolocation.lat}, ${data.address.geolocation.long})`;
    addressDiv.appendChild(addressHeading);
    addressDiv.appendChild(address);
    
    let phone = document.createElement('h3');
    phone.textContent = `Phone: ${data.phone}`;
    
    description.appendChild(userID);
    description.appendChild(userName);
    description.appendChild(email);
    description.appendChild(addressDiv);
    description.appendChild(phone); 

    userDiv.appendChild(userFullName);
    userDiv.appendChild(description);

    let users = document.querySelector('.users');
    users.appendChild(userDiv);
}

